import re
from bs4 import BeautifulSoup, SoupStrainer


def parse_category_page(content, item):
    # use soup strainer and lxml to reduce parse time
    #   only parse elements that have an href matching what we want
    only_links = SoupStrainer(href=re.compile('/shop/' + item.cat))
    parse = BeautifulSoup(content, 'lxml', parse_only=only_links)
    # get lists of urls from tags that match the item name and color #
    links_matching_name = set([x.get('href') for x in parse.find_all(text=item.name, href=True)])
    links_matching_color = set([x.get('href') for x in parse.find_all(text=item.color, href=True)])
    # intersect sets - item found if one link is intersected
    intersect = links_matching_name & links_matching_color

    # TODO - implement error handling
    if len(intersect) == 1:
        item.url = intersect.pop()


def parse_atc_form(content, item):
    # TODO - check for correct form
    # limit the parser to only parse the form
    form_parse = BeautifulSoup(content, "lxml", parse_only=SoupStrainer("form"))
    # grab post data values from input tags
    item.atc_form_data = [(x.get('name'), x.get('value')) for x in form_parse.find_all("input")]
    # try to find size - if no match found default to pop()
    select_size_options = form_parse.find_all('option')
    if len(select_size_options) > 0:
        for opt in select_size_options:
            if opt.text == item.size:
                item.atc_form_data.append(('s', opt.get('value')))
                break
        else:
            item.atc_form_data.append(('s', select_size_options.pop().get('value')))
    item.atc_url = form_parse.form.get('action')