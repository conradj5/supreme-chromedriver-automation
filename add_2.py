import time
import logging
import requests
from urllib.parse import urljoin
from selenium import webdriver

from item import Item
import parse

get_time = lambda: int(round(time.time() * 1000))


def run_test():
    # TESTING ITEM INFO #
    item = Item()

    # set up logger #
    log = logging.getLogger("SupDev")
    log_format = logging.Formatter("[%(asctime)s.%(msecs)03d] %(message)s", "%H:%M:%S")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_format)
    log.addHandler(console_handler)
    log.setLevel(logging.DEBUG)

    base = "http://www.supremenewyork.com/shop/all/"

    # create session - should probably modify to obfuscate requests library usage #
    log.info('Initiating driver and session')
    driver = webdriver.Chrome('./chromedriver')
    driver.get('http://www.supremenewyork.com/shop/all')
    session = requests.session()
    session.headers.update({
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36'
                      '(KHTML, like Gecko) Chrome/56.0.2924.28 Safari/537.36'})

    log.info('Ready')
    input("Enter to start\n")
    start = get_time()
    log.info('Starting item search')

    # get category page #
    log.info("Getting category page")
    response = session.get(base + item.cat)

    # parse page for matching item link #
    log.info("Parsing category page")
    parse.parse_category_page(response.content.lower(), item)

    # get parsed item page #
    log.info("Getting item page " + item.url)
    response = session.get(urljoin(base, item.url))

    # parse ATC form for item info #
    log.info("Parsing item page")
    parse.parse_atc_form(response.content, item)

    # send ATC POST request #
    log.info('Adding to cart')
    headers = {"Content-Type": "application/x-www-form-urlencoded", "Accept": "text/javascript"}
    session.post(urljoin(base, item.atc_url), data=item.atc_form_data, headers=headers)

    # set the cookie in the webdriver #
    log.info('Setting driver cookies')
    driver.delete_all_cookies()
    driver.add_cookie({'name': '_supreme_sess', 'value': session.cookies['_supreme_sess']})

    # go to checkout page
    log.info('Downloading checkout page')
    driver.get('https://www.supremenewyork.com/checkout')
    log.info('Done')
    print(str(get_time() - start) + "ms\n")

    input('Enter to exit')
    driver.quit()

run_test()

