import time
import logging
import requests
from re import compile
from urllib.parse import urljoin
from bs4 import BeautifulSoup, SoupStrainer
from selenium import webdriver

get_time = lambda: int(round(time.time() * 1000))

# TESTING ITEM INFO #
item_name = "work pant"
item_color = "light green"
item_size = "30"
item_cat = "pants"

# set up logger #
log = logging.getLogger("SupDev")
logFormat = logging.Formatter("[%(asctime)s.%(msecs)03d] %(message)s", "%H:%M:%S")
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormat)
log.addHandler(consoleHandler)
log.setLevel(logging.DEBUG)

base = "http://www.supremenewyork.com/shop/all/"

# create session - should probably modify to obfuscate requests library usage #
log.info('Initiating driver and session')
driver = webdriver.Chrome('./chromedriver')
driver.get('http://www.supremenewyork.com/shop/all')
session = requests.session()
session.headers.update({
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36'
                  '(KHTML, like Gecko) Chrome/56.0.2924.28 Safari/537.36'})

log.info('Ready')
input("Enter to start\n")
start = get_time()
log.info('Starting item search')

# get category page #
log.info("Getting category page")
response = session.get(base + item_cat)

# parse page for matching item link #
log.info("Parsing category page")
# use soup strainer and lxml to reduce parse time - only parse elements that have an href matching what we want
only_links = SoupStrainer(href=compile('/shop/' + item_cat))
parse = BeautifulSoup(response.content.lower(), 'lxml', parse_only=only_links)
# get lists of urls from tags that match the item name and color #
links_name = set([x.get('href') for x in parse.find_all(text=item_name, href=True)])
links_color = set([x.get('href') for x in parse.find_all(text=item_color, href=True)])
# intersect sets - item found if one link is intersected
intersect = links_name & links_color
if len(intersect) != 1:
    log.info('Item match not found')
    exit(1)

item_url = intersect.pop()

# get item page #
log.info("Getting item page - " + item_url)
response = session.get(urljoin(base, item_url))

# parse form for item info #
log.info("Parsing item page")
only_forms = SoupStrainer("form")  # limit the parser to only parse the form
form_parse = BeautifulSoup(response.content, "lxml", parse_only=only_forms)
# grab post data values from input tags
atc_form_data = [(x.get('name'), x.get('value')) for x in form_parse.find_all("input")]
# try to find size - if no match found default to pop()
select_size_options = form_parse.find_all('option')
if len(select_size_options) > 0:
    for opt in select_size_options:
        if opt.text == item_size:
            log.info('Size ' + opt.text + ' selected')
            atc_form_data.append(('s', opt.get('value')))
            break
    else:
        log.info("Size not found - defaulting")
        atc_form_data.append(('s', select_size_options.pop().get('value')))

atc_url = form_parse.form.get('action')

# send ATC POST request #
log.info('Adding to cart')
headers = {"Content-Type": "application/x-www-form-urlencoded", "Accept": "text/javascript"}
atc_response = session.post(urljoin(base, atc_url), data=atc_form_data, headers=headers)

# set the cookie in the webdriver
log.info('Setting driver cookies')
driver.delete_all_cookies()
driver.add_cookie({'name': '_supreme_sess', 'value': session.cookies['_supreme_sess']})

# go to checkout page
log.info('Downloading checkout page')
driver.get('https://www.supremenewyork.com/checkout')
log.info('Done')
print(str(get_time() - start) + "ms\n")

input('Enter to exit')
driver.quit()
